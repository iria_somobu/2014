package com.iillyyaa2033.fourteen;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.iillyyaa2033.fourteen.model.Category;
import com.iillyyaa2033.fourteen.model.Methods;
import com.iillyyaa2033.fourteen.model.Post;
import com.iillyyaa2033.fourteen.model.Tag;
import com.iillyyaa2033.fourteen.routing.Page;
import com.iillyyaa2033.fourteen.routing.PostsListPage;
import com.iillyyaa2033.fourteen.routing.Router;
import com.iillyyaa2033.fourteen.routing.SinglePostPage;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.net.URI;
import java.sql.Driver;
import java.sql.DriverManager;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends CustomizedActivity implements AndroidUtils.LinkClick {

    static Typeface lato, lato_light, lato_bold;

    static {
        try {
            DriverManager.registerDriver((Driver) Class.forName("org.sqldroid.SQLDroidDriver").newInstance());
        } catch (Exception e) {
            throw new RuntimeException("Failed to register SQLDroidDriver");
        }

        Platform.setImplementation(new IPlatform() {

            @Override
            public String getJdbcPath(String name) {
                return "jdbc:sqldroid:" + Const.EXT_ROOT + "/" + name + "/main.db";
            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (lato == null) lato = Typeface.createFromAsset(getAssets(), "lato_regular.ttf");
        if (lato_light == null)
            lato_light = Typeface.createFromAsset(getAssets(), "lato_light.ttf");
        if (lato_bold == null) lato_bold = Typeface.createFromAsset(getAssets(), "lato_bold.ttf");

        if (!PreferenceManager.getDefaultSharedPreferences(this).getBoolean("STARTUP_DONE", false)) {
            startActivity(new Intent(this, BlogSettingsActivity.class));
        } else {

            Uri uri = getIntent().getData();
            if (uri != null) {

                if (!processMyLink(URI.create(uri.toString())))
                    openLink(URI.create(uri.toString()));

            } else {
                // Here we're opening local blog on first page
                processMyLink(URI.create(Utils.getUri(Const.LOCAL_BLOG, Methods.VIEW_BLOG, 1).toString()));
            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ParallaxView parallaxView = findViewById(R.id.parallax);
        if (parallaxView != null) parallaxView.destroy();
    }

    @Override
    public void onBackPressed() {

        Uri uri = getIntent().getData();
        if (uri != null) {
            URI viewCurrentBlog = Utils.getUri(Const.LOCAL_BLOG, Methods.VIEW_BLOG, 1);
            if (!uri.toString().equals(viewCurrentBlog.toString())) {
                openLink(viewCurrentBlog);
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
            }
        }
    }

    private boolean processMyLink(URI uri) {
        System.out.println("Processing link " + uri.toString());

        Page page = Router.route(uri);
        if (page instanceof PostsListPage) {
            showPostsList((PostsListPage) page);
            return true;
        } else if (page instanceof SinglePostPage) {
            showSinglePost((SinglePostPage) page);
            return true;
        }

        return false;
    }

    private void openLink(URI uri) {
        String method = uri.getPath();

        if (method != null && method.length() > 1) {

            Class<? extends Activity> aClass = null;

            method = method.substring(1);
            switch (method) {
                case Methods.VIEW_BLOG:
                case Methods.VIEW_POST:
                case Methods.VIEW_DATE:
                case Methods.VIEW_CATEGORY:
                    aClass = MainActivity.class;
                    break;

                case Methods.LIST_BLOGS:
                    aClass = BlogsExplorerActivity.class;
                    break;

                case Methods.ADD_POST:
                    aClass = EditorActivity.class;
                    break;

                case Methods.EDIT_BLOG:
                    aClass = BlogSettingsActivity.class;
                    break;
                case Methods.EDIT_POST:
                    aClass = EditorActivity.class;
                    break;

                default:
                    Toast.makeText(MainActivity.this, getString(R.string.err_url_not_ready_yet) + " " + uri.toString(), Toast.LENGTH_SHORT).show();
            }

            if (aClass != null) {
                startActivity(new Intent(this, aClass).setData(Uri.parse(uri.toString())));
            }


        } else {
            Toast.makeText(MainActivity.this, R.string.err_method_not_found, Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    // ============= //
    // SCREENS SETUP //
    // ============= //
    void showPostsList(PostsListPage page) {
        setContentView(R.layout.activity_main);
        setupHeader(page);

        LinearLayout primaryLayout = findViewById(R.id.primary);

        switch (page.message) {
            case FILTER_BY_DAY:
                primaryLayout.addView(getPageFilterInfo(R.string.filter_by_day,
                        SimpleDateFormat.getDateInstance().format(page.datefilter)));
                break;
            case FILTER_BY_MONTH:
                primaryLayout.addView(getPageFilterInfo(R.string.filter_by_month,
                        new SimpleDateFormat("MMM yyyy", Locale.getDefault()).format(page.datefilter)));
                break;
            case FILTER_BY_CATEGORY:
                primaryLayout.addView(getPageFilterInfo(R.string.filter_by_cat, page.catname));
                break;
        }

        List<Post> posts = page.posts;
        if (posts.isEmpty()) {
            primaryLayout.addView(getNoPost());
        } else {
            for (Post post : posts) primaryLayout.addView(getPostItem(post));
            primaryLayout.addView(getPageNavigation(page));
        }

        setupWhiteWidgets(page);
        setupBlackWidgets(page);
        setupFooter(page);
        setupParallax();
    }

    void showSinglePost(SinglePostPage page) {
        setContentView(R.layout.activity_main);
        setupHeader(page);

        LinearLayout primary = findViewById(R.id.primary);
        primary.addView(getPost(page.post));

        primary.addView(getPostNavigation(page.prev, page.next));

        setupWhiteWidgets(page);
        setupBlackWidgets(page);
        setupFooter(page);
        setupParallax();
    }

    // ================ //
    // COMPONENTS SETUP //
    // ================ //
    private void setupHeader(Page page) {
        final String blog = page.blog;
        String blog_name = page.blog_name;
        boolean editable = page.editable;

        ImageView headerLogo = findViewById(R.id.header_logo);
        File logo = new File(Const.EXT_ROOT, "/" + blog + "/header_image.jpg");
        if (logo.exists()) {
            Bitmap bm = BitmapFactory.decodeFile(logo.getAbsolutePath());
            headerLogo.setImageBitmap(bm);
        }

        String name = blog_name;
        if (name == null || name.isEmpty()) name = getString(R.string.default_blog_name);
        TextView blogName = findViewById(R.id.blog_name);
        blogName.setText(name);
        blogName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLink(Utils.getUri(blog, Methods.VIEW_BLOG, 1));
            }
        });

        findViewById(R.id.btn_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View searchbox = findViewById(R.id.searchbox);
                searchbox.setVisibility(searchbox.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            }
        });

        findViewById(R.id.btn_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View menubox = findViewById(R.id.menubox);
                menubox.setVisibility(menubox.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE);
            }
        });

        ((EditText) findViewById(R.id.searchfield)).setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                Toast.makeText(MainActivity.this, "Todo: fulltext search", Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        LinearLayout menu = findViewById(R.id.menubox);
        if (editable)
            menu.addView(getMenuItem(R.string.menu_addpost, Utils.getUri(blog, Methods.ADD_POST, null, null)));
        if (editable)
            menu.addView(getMenuItem(R.string.menu_editblog, Utils.getUri(blog, Methods.EDIT_BLOG, null, null)));
        menu.addView(getMenuItem(R.string.menu_listblogs, Utils.getUri(blog, Methods.LIST_BLOGS, null, null)));
        //        menu.addView(getMenuItem("...",         Utils.getUri(current_blog,null,null,null)));
    }

    private void setupFooter(Page page) {
        // Nothing for now
    }

    private void setupWhiteWidgets(Page page) {
        LinearLayout ll = findViewById(R.id.widgets_white);
        ll.addView(getCalendarWidget(page.blog, page.postsByDay));
    }

    private void setupBlackWidgets(Page page) {
        // TODO: yet to implement
    }

    private void setupParallax() {
        ScrollView scroll = findViewById(R.id.scroll);
        ParallaxView parallax = findViewById(R.id.parallax);
        if (scroll == null || parallax == null) return;

        //        parallax.bindScrollView(scroll);
        //        parallax.addScaledLayer(R.drawable.header_image);
    }

    private View getMenuItem(int titleRes, final URI target) {
        View root = LayoutInflater.from(this).inflate(R.layout.part_menuitem, null, false);

        TextView text = root.findViewById(R.id.text);
        text.setText(titleRes);
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLink(target);
            }
        });

        return root;
    }

    private View getPostItem(final Post post) {
        return _getPostOrPostItem(R.layout.part_postitem, post);
    }

    private View getPost(final Post post) {
        return _getPostOrPostItem(R.layout.part_post, post);
    }

    private View getNoPost() {
        return LayoutInflater.from(this).inflate(R.layout.part_no_post, null, false);
    }

    private View _getPostOrPostItem(int layout, final Post post) {
        View root = LayoutInflater.from(this).inflate(layout, null, false);

        TextView content = root.findViewById(R.id.simple_content);
        if (content != null) content.setText(Html.fromHtml(post.post));

        ImageView headerImage = root.findViewById(R.id.header_image);
        if (headerImage != null) {
            if (post.header_image != null) {
                headerImage.setVisibility(View.VISIBLE);
                File f = new File(Const.EXT_ROOT, post.blog + "/" + post.header_image);
                Picasso.get()
                        .load(f)
                        .memoryPolicy(MemoryPolicy.NO_CACHE)
                        .resize(1920, 1080)
                        .centerCrop()
                        .into(headerImage);
            } else {
                headerImage.setVisibility(View.GONE);
                View divider = root.findViewById(R.id.divider);
                if (divider != null) divider.setVisibility(View.VISIBLE);

                // Reset magrings for bigger post view
                LinearLayout ll = root.findViewById(R.id.layout_offset);
                if (ll != null) {
                    LinearLayout.LayoutParams p = (LinearLayout.LayoutParams) ll.getLayoutParams();
                    p.setMargins(0, 0, 0, 0);
                    ll.setLayoutParams(p);
                }
            }
        }

        TextView categories = root.findViewById(R.id.categories);
        if (categories != null) {
            String cats = formatCategoriesString(post.blog, post.cats);
            if (cats.isEmpty()) categories.setVisibility(View.GONE);
            else AndroidUtils.setClickableText(categories, cats, this);
        }

        TextView tags = root.findViewById(R.id.tags);
        if (tags != null) {
            String tagsStr = formatTagsString(post.tags);
            if (tagsStr.isEmpty()) tags.setVisibility(View.GONE);
            else AndroidUtils.setClickableText(tags, tagsStr, this);
        }

        TextView title = root.findViewById(R.id.title);
        if (title != null) {
            title.setText(post.title);
            title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openLink(Utils.getUri(post.blog, Methods.VIEW_POST, post.id));
                }
            });
            title.setTypeface(content != null ? lato_light : lato_bold);
        }

        TextView info = root.findViewById(R.id.info);
        if (info != null) {

            Calendar c = Calendar.getInstance();
            c.setTimeInMillis(post.time);
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);

            String date = String.format(Utils.URL, Utils.getUri(post.blog, Methods.VIEW_DATE, "mode=day&since=" + c.getTimeInMillis(), "1"), Utils.getDate(post.time, "dd.MM.yyyy"));
            String user = String.format(Utils.URL, Utils.getUri(post.blog, Methods.VIEW_USER, post.author_id), post.author_pubname);
            String edit = String.format(Utils.URL, Utils.getUri(post.blog, Methods.EDIT_POST, post.id), getString(R.string.post_edit));

            String total = date + " | " + user;
            if (post.editable) total += " | " + edit;

            AndroidUtils.setClickableText(info, total, this);
            info.setTypeface(lato);
        }

        TextView source = root.findViewById(R.id.source);
        if (source != null) {
            if (post.source != 0) {
                source.setText(getString(R.string.source, post.source_name));
            } else {
                source.setVisibility(View.GONE);
            }
        }

        return root;
    }

    private View getPageNavigation(PostsListPage page) {
        return getPageNavigation(page.blog, page.page, (page.total / 10) + (page.total % 10 > 0 ? 1 : 0));
    }

    private View getPageNavigation(String blog, long current, long count) {
        View root = LayoutInflater.from(this).inflate(R.layout.part_pagenav, null, false);

        if (count > 1) {
            boolean hasNext = count - current > 0;
            boolean hasPrev = current > 1;
            if (!hasNext) root.findViewById(R.id.next).setVisibility(View.GONE);
            if (!hasPrev) root.findViewById(R.id.prev).setVisibility(View.GONE);

            if (current <= 3) root.findViewById(R.id.first_skip).setVisibility(View.GONE);
            if (current <= 2) root.findViewById(R.id.first).setVisibility(View.GONE);
            if (current <= 1) root.findViewById(R.id.center_pre).setVisibility(View.GONE);

            if (current > count - 3) root.findViewById(R.id.last_skip).setVisibility(View.GONE);
            if (current > count - 2) root.findViewById(R.id.last).setVisibility(View.GONE);
            if (current > count - 1) root.findViewById(R.id.center_post).setVisibility(View.GONE);

            subscribeForNav(blog, (TextView) root.findViewById(R.id.prev), current - 1, false);
            subscribeForNav(blog, (TextView) root.findViewById(R.id.first), 1, true);
            subscribeForNav(blog, (TextView) root.findViewById(R.id.center_pre), current - 1, true);
            subscribeForNav(blog, (TextView) root.findViewById(R.id.center), current, true);
            subscribeForNav(blog, (TextView) root.findViewById(R.id.center_post), current + 1, true);
            subscribeForNav(blog, (TextView) root.findViewById(R.id.last), count, true);
            subscribeForNav(blog, (TextView) root.findViewById(R.id.next), current + 1, false);
        } else {
            root.setVisibility(View.GONE);
        }

        return root;
    }

    private View getPageFilterInfo(int id, String... args) {
        View v = LayoutInflater.from(this).inflate(R.layout.part_filterinfo, null, false);

        ((TextView) v.findViewById(R.id.filterinfo_text)).setText(Html.fromHtml(getString(id, args)));

        return v;
    }

    private View getPostNavigation(final Post prev, final Post next) {
        View root = LayoutInflater.from(this).inflate(R.layout.part_postnav, null, false);

        if (prev == null) {
            root.findViewById(R.id.prev_root).setVisibility(View.GONE);
            root.findViewById(R.id.inner_divider).setVisibility(View.GONE);
        } else {
            ((TextView) root.findViewById(R.id.link_prev)).setText(prev.title);
            root.findViewById(R.id.prev_root).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    openLink(Utils.getUri(prev.blog, Methods.VIEW_POST, prev.id));
                }
            });
        }

        if (next == null) {
            root.findViewById(R.id.next_root).setVisibility(View.GONE);
            root.findViewById(R.id.inner_divider).setVisibility(View.GONE);
        } else {
            ((TextView) root.findViewById(R.id.link_next)).setText(next.title);
            root.findViewById(R.id.next_root).setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    openLink(Utils.getUri(next.blog, Methods.VIEW_POST, next.id));
                }
            });
        }

        return root;
    }


    private View getCalendarWidget(String blog, int[] cnt) {
        View root = LayoutInflater.from(this).inflate(R.layout.widget_calendar, null, false);
        TableLayout table = root.findViewById(R.id.calendar_table);

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        int firstDayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int lastDayOfMonth = calendar.getMaximum(Calendar.DAY_OF_MONTH);

        long startMillis = calendar.getTimeInMillis();
        long dayInMillis = 24 * 60 * 60 * 1000;

        DateFormat dateFormat = new SimpleDateFormat("LLLL YYYY", Locale.getDefault());
        String s = dateFormat.format(new Date(System.currentTimeMillis()));
        ((TextView) root.findViewById(R.id.calendar_month)).setText(s);

        int currentDay = 1 - firstDayOfWeek;
        for (int i = 1; i < 7; i++) {

            TableRow row = (TableRow) table.getChildAt(i);
            if (currentDay >= lastDayOfMonth) row.setVisibility(View.GONE);

            for (int j = 0; j < 7; j++) {

                currentDay++;

                boolean clickable = false;
                if (currentDay > 0 && currentDay < cnt.length - 1)
                    clickable = cnt[currentDay + 1] >= 1;

                TextView text = (TextView) row.getChildAt(j);
                if (currentDay > 0 && currentDay <= lastDayOfMonth) {
                    if (clickable) {
                        String link = Utils.getUri(blog, Methods.VIEW_DATE, "mode=day&since=" + (startMillis + currentDay * dayInMillis), "1").toString();
                        AndroidUtils.setClickableText(text, "<a href =\"" + link + "\">" + currentDay + "</a>", this);
                        text.setBackgroundResource(R.drawable.sl_searchbutton);
                    } else {
                        text.setText("" + currentDay);
                    }
                } else {
                    text.setText("");
                }
            }
        }

        LinearLayout calNavRoot = findViewById(R.id.calendar_page);
        // TODO: setup calendar navigation

        return root;
    }

    private View getArchivesWidget(FourteenDB db) {
        View root = LayoutInflater.from(this).inflate(R.layout.widget_archives, null, false);
        LinearLayout ll = root.findViewById(R.id.llayout);

        SimpleDateFormat sdf = new SimpleDateFormat("MMMM YYYY", Locale.getDefault());

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);


        for (int year : db.getYearsWithPosts()) {
            calendar.set(Calendar.YEAR, year);

            int[] postsInMonths = db.getPostsCountByYear(year);
            for (int month = 0; month < 12; month++) {
                calendar.set(Calendar.MONTH, month);

                int postsInMonth = postsInMonths[month];

                if (postsInMonth > 0) {

                    View itmRoot = LayoutInflater.from(this).inflate(R.layout.part_archives_item, null, false);
                    TextView text = itmRoot.findViewById(R.id.item);

                    String link = Utils.getUri(db.blog, Methods.VIEW_DATE, "mode=month&since=" + calendar.getTimeInMillis(), "1").toString();
                    AndroidUtils.setClickableText(text, "<a href =\"" + link + "\">" + sdf.format(calendar.getTime()) + "</a>", this);

                    ll.addView(itmRoot);


                    View divider = new View(this);
                    divider.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 1));
                    divider.setBackgroundColor(Color.argb(127, 255, 255, 255));
                    ll.addView(divider);
                }
            }
        }

        if (ll.getChildCount() >= 3) {
            ll.removeViewAt(ll.getChildCount() - 1);
        }

        return root;
    }

    private View getCategoriesWidget() {
        TextView root = new TextView(this);
        root.setText("TODO: Categories");
        return root;
    }

    /**
     * Make given TextView open URI
     *
     * @param view
     * @param target
     * @param overrideText
     */
    private void subscribeForNav(final String blog, TextView view, final long target, boolean overrideText) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLink(Utils.getUri(blog, Methods.VIEW_BLOG, null, "" + target));
            }
        });

        if (overrideText) {
            view.setText("" + target);
        }
    }

    private String formatCategoriesString(String blog, Category[] cats) {
        String out = "";

        for (int i = 0; i < cats.length; i++) {
            out += String.format(Utils.URL, Utils.getUri(blog, Methods.VIEW_CATEGORY, "cat=" + cats[i].categoryId, "1"), cats[i].categoryName);
            if (i != cats.length - 1) out += ", ";
        }

        return out;
    }

    private String formatTagsString(Tag[] tags) {
        String out = "";

        for (int i = 0; i < tags.length; i++) {
            out += tags[i].nicename;
            if (i != tags.length - 1) out += ", ";
        }

        return out;
    }

    @Override
    public void onLinkClick(String url) {
        openLink(URI.create(url));
    }
}
