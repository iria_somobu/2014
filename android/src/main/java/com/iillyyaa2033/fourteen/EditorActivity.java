package com.iillyyaa2033.fourteen;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.iillyyaa2033.fourteen.model.Category;
import com.iillyyaa2033.fourteen.model.Methods;
import com.iillyyaa2033.fourteen.model.Post;
import com.jkcarino.rtexteditorview.RTextEditorToolbar;
import com.jkcarino.rtexteditorview.RTextEditorView;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;

public class EditorActivity extends CustomizedActivity {

    private static final int CODE_PICK_IMG = 1;

    private Post currentPost = null;
    private String blog;

    private EditText title;
    private RTextEditorView editor;
    private String headerImage;
    private ArrayList<Long> assignedCats = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);

        // Setup main panel
        editor = findViewById(R.id.editor_view);
        editor.setIncognitoModeEnabled(true);

        title = findViewById(R.id.title);

        RTextEditorToolbar editorToolbar = findViewById(R.id.editor_toolbar);
        editorToolbar.setEditorView(editor);

        TabHost tabHost = findViewById(R.id.host);
        if(tabHost != null) {
            tabHost.setup();

            TabHost.TabSpec tabSpec = tabHost.newTabSpec("post");
            tabSpec.setContent(R.id.panel_data);
            tabSpec.setIndicator(getString(R.string.editor_panel_post));
            tabHost.addTab(tabSpec);

            tabSpec = tabHost.newTabSpec("meta");
            tabSpec.setContent(R.id.panel_meta_root);
            tabSpec.setIndicator(getString(R.string.editor_panel_meta));
            tabHost.addTab(tabSpec);

            tabHost.setCurrentTab(0);
        }


        // Process link
        Uri uri = getIntent().getData();
        if(uri != null){
            processLink(URI.create(uri.toString()));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(resultCode == RESULT_OK){
            headerImage = data.getStringExtra("image");

            if(currentPost != null)
                currentPost.header_image = headerImage;

            updateBlockImage();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        destroyEditor();
    }

    private void destroyEditor(){
        if(editor != null){
            editor.removeAllViews();
            editor.destroy();
            editor = null;
        }
    }

    private void processLink(URI uri){
        blog = uri.getAuthority();
        String method = uri.getPath();
        String params = uri.getQuery();
        String fragment = uri.getFragment();

        if(method != null && method.length() > 1){
            method = method.substring(1);
            switch(method){
                case Methods.ADD_POST:
                    // Just open this activity
                    setEditionContent(null);
                    setupBlocks();
                    break;
                case Methods.EDIT_POST:
                    FourteenDB db = FourteenDB.open(this, blog);
                    Post post = db.getPostById(Utils.parseLong(fragment, -1));
                    db.close();
                    setEditionContent(post);
                    setupBlocks();
                    break;
                default:
                    Toast.makeText(EditorActivity.this,"Todo: process link ["+blog+"|"+method+"|"+params+"|"+fragment+"]", Toast.LENGTH_SHORT).show();
                    finish();
            }
        } else {
            Toast.makeText(EditorActivity.this,"Url broken: method not found", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    private void setEditionContent(Post post){
        currentPost = post;

        if(post != null){
            title.setText(post.title);
            editor.setHtml(post.post.replace("'","&lsquo;"));
            headerImage = post.header_image;
        } else {
            title.setText("");
            editor.setHtml("<p></p>");
        }
    }


    // ============= //
    // EDITOR BLOCKS //
    // ============= //
    private void setupBlocks(){
        // Setup second panel
        LinearLayout panelMeta = findViewById(R.id.panel_meta);

        setupBlockPublish(panelMeta);
        setupBlockFormat(panelMeta);
        setupBlockCats(panelMeta);
        setupBlockTags(panelMeta);
        setupBlockImage(panelMeta);
    }

    private View surroundWithBlockframe(int title, int block_id){
        View root = LayoutInflater.from(this).inflate(R.layout.part_editorblock, null, false);
        View block = LayoutInflater.from(this).inflate(block_id, null, false);

        ((TextView) root.findViewById(R.id.content_title)).setText(title);

        final LinearLayout ll = root.findViewById(R.id.content);
        if(block != null) ll.addView(block);

        final ImageView collapser = root.findViewById(R.id.collapser);
        collapser.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if(ll.getVisibility() == LinearLayout.VISIBLE){
                    collapser.setImageResource(android.R.drawable.arrow_up_float);
                    ll.setVisibility(LinearLayout.GONE);
                } else {
                    collapser.setImageResource(android.R.drawable.arrow_down_float);
                    ll.setVisibility(LinearLayout.VISIBLE);
                }

            }
        });

        return root;
    }

    private void setupBlockPublish(LinearLayout panelMeta) {

        View blockPublish = surroundWithBlockframe(R.string.editor_block_publish, R.layout.block_publish);
        blockPublish.findViewById(R.id.block_btn_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delete();
            }
        });
        blockPublish.findViewById(R.id.block_btn_post).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                post();
            }
        });

        if(currentPost != null) {
            blockPublish.findViewById(R.id.block_btn_delete).setEnabled(true);
        }

        panelMeta.addView(blockPublish);

    }

    private void setupBlockFormat(LinearLayout panelMeta){
//        View blockFormat = surroundWithBlockframe(R.string.editor_block_format, R.layout.block_);
//        panelMeta.addView(blockFormat);
    }

    private void setupBlockCats(LinearLayout panelMeta) {
        final View blockCategories = surroundWithBlockframe(R.string.editor_block_category, R.layout.block_category);

        blockCategories.findViewById(R.id.text_add_cat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LinearLayout ll = blockCategories.findViewById(R.id.new_cat_root);

                if(ll.getVisibility() == LinearLayout.VISIBLE){
                    ll.setVisibility(LinearLayout.GONE);
                } else {
                    ll.setVisibility(LinearLayout.VISIBLE);
                }
            }
        });

        blockCategories.findViewById(R.id.new_cat_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText field = blockCategories.findViewById(R.id.new_cat_text);
                String catName = field.getText().toString();
                String codeName = catName.toLowerCase().trim().replaceAll(" ,\\.\\*", "-");
                Category cat = new Category(codeName, catName);
                FourteenDB db = FourteenDB.open(EditorActivity.this, blog);
                db.addOrUpdateCategoryByCodename(cat);
                db.close();

                blockCategories.findViewById(R.id.new_cat_root).setVisibility(View.GONE);
                field.setText("");

                updateBlockCats(blog);
            }
        });

        panelMeta.addView(blockCategories);

        updateBlockCats(blog);
    }

    private void setupBlockTags(LinearLayout panelMeta){
        View blockTags = surroundWithBlockframe(R.string.editor_block_tags, R.layout.block_tags);
        panelMeta.addView(blockTags);
    }

    private void setupBlockImage(LinearLayout panelMeta) {
        View blockImage = surroundWithBlockframe(R.string.editor_block_image, R.layout.block_image);
        panelMeta.addView(blockImage);

        ImageView image = blockImage.findViewById(R.id.image_view);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(EditorActivity.this, GalleryActivity.class), CODE_PICK_IMG);
            }
        });

        findViewById(R.id.image_del).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(currentPost != null)
                    currentPost.header_image = null;

                headerImage = null;

                updateBlockImage();
            }
        });

        updateBlockImage();
    }

    private void updateBlockCats(String blog){
        FourteenDB db = FourteenDB.open(this, blog);
        final Category[] categories = db.getAllCategories();


        if(currentPost != null) {
            for (Category cat : db.getAssignedCategoriesByPost(currentPost.id)) {
                if(!assignedCats.contains(cat.categoryId))
                    assignedCats.add(cat.categoryId);
            }
        }
        db.close();

        LinearLayout root = findViewById(R.id.cat_root);
        root.removeAllViews();
        if(categories.length < 1){
            TextView text = new TextView(this);
            text.setText(R.string.editor_cat_not_yet);
            root.addView(text);
        } else {

            for(int i = 0; i < categories.length; i++){
                final int e = i;

                CheckBox cb = new CheckBox(this);
                cb.setText(categories[i].categoryName);

                if(assignedCats.contains(categories[i].categoryId)) {
                    cb.setChecked(true);
                }

                cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if(b) assignedCats.add(categories[e].categoryId);
                        else assignedCats.remove(categories[e].categoryId);
                    }
                });

                root.addView(cb);
            }

        }
    }

    private void updateBlockImage(){
        if(headerImage != null) {
            Picasso.get()
                    .load(new File(Const.EXT_ROOT, blog+"/"+headerImage))
                    .memoryPolicy(MemoryPolicy.NO_CACHE)
                    .resize(1920, 1080)
                    .centerCrop()
                    .into((ImageView) findViewById(R.id.image_view));

            findViewById(R.id.image_action).setVisibility(View.GONE);
            findViewById(R.id.image_del).setVisibility(View.VISIBLE);
        } else {
            ((ImageView) findViewById(R.id.image_view)).setImageResource(android.R.drawable.ic_menu_report_image);
            findViewById(R.id.image_action).setVisibility(View.VISIBLE);
            findViewById(R.id.image_del).setVisibility(View.GONE);
        }
    }


    // ================== //
    // FINAL POST ACTIONS //
    // ================== //
    private void delete(){
        if(currentPost == null) return;

        new AlertDialog.Builder(this)
                .setTitle(R.string.editor_warning)
                .setMessage(R.string.editor_undone)
                .setNegativeButton(android.R.string.cancel, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        FourteenDB.open(EditorActivity.this, blog).deletePost(currentPost.id).close();
                        startActivity(new Intent(EditorActivity.this, MainActivity.class));
                    }
                })
                .show();
    }

    private void post(){
        String title = this.title.getText().toString();
        String content = editor.getHtml()
                .replace("'","&lsquo;");

        if(title.isEmpty()) title = getString(R.string.editor_no_title);

        FourteenDB db = FourteenDB.open(this, blog);

        long postID;
        if(currentPost == null) {
            postID = db.insertSimpleRecord(title, content, System.currentTimeMillis(), headerImage);
        } else {
            currentPost.title = title;
            currentPost.post = content;
            db.updatePost(currentPost);
            postID = currentPost.id;
        }

        db.reassignCats(postID, assignedCats.toArray(new Long[assignedCats.size()]));

        db.close();

        Uri uri = Uri.parse(Utils.getUri(blog, Methods.VIEW_BLOG, 1).toString());
        startActivity(new Intent(this, MainActivity.class).setData(uri));
    }
}
