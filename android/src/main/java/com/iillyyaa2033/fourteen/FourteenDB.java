package com.iillyyaa2033.fourteen;

import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.iillyyaa2033.fourteen.model.Category;
import com.iillyyaa2033.fourteen.model.ImportedPost;
import com.iillyyaa2033.fourteen.model.Post;
import com.iillyyaa2033.fourteen.model.Source;
import com.iillyyaa2033.fourteen.model.Tag;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class FourteenDB {
    private static final int POSTS_PER_PAGE = 10;

    public String blog;
    public boolean editable;

    private SQLiteDatabase db;

    private FourteenDB(SQLiteDatabase db) {
        this.db = db;
    }

    public static FourteenDB open(Context context, String name) {
        SQLiteDatabase database;
        boolean editable;

        if (Utils.isDbEditable(name)) {
            database = new OpenHelper(context, name).getWritableDatabase();
            editable = true;
        } else {
            database = new OpenHelper(context, name).getReadableDatabase();
            editable = false;
        }

        FourteenDB db = new FourteenDB(database);
        db.blog = name;
        db.editable = editable;
        return db;
    }

    public void close() {
        db.close();
    }


    // ======== //
    // SETTINGS //
    // ======== //
    void updateSetting(String key, String value) {
        if (value == null) value = "";

        String lastVal = getSetting(key);

        ContentValues cv = new ContentValues();
        cv.put("pval", value);

        if (lastVal == null) {
            cv.put("pkey", key);
            db.insert("settings", null, cv);
        } else {
            db.update("settings", cv, "pkey = '" + key + "'", null);
        }
    }

    String getSetting(String name) {
        Cursor cursor = db.query(
                "settings", new String[]{"pval"},
                "pkey = '" + name + "'", null,
                null, null, "pval ASC limit 1"
        );

        String rz = null;

        if (cursor.moveToNext()) {
            rz = cursor.getString(0);
        }

        cursor.close();

        return rz;
    }


    // ===== //
    // POSTS //
    // ===== //
    private static final String[] POST_PROJECTION = new String[]{
            "_id", "source", "title", "post", "time", "author", "header_img"
    };

    long insertSimpleRecord(String title, String content, long time, String headerImg) {
        return insertSimpleRecord(0, title, content, time, headerImg);
    }

    long insertSimpleRecord(long source, String title, String content, long time, String headerImg) {

        ContentValues cv = new ContentValues();
        cv.put("source", source);
        cv.put("title", title);
        cv.put("post", content);
        cv.put("time", time);
        if (headerImg != null) cv.put("header_img", headerImg);
        long id = db.insert("posts", null, cv);

        increaseDayPostCounter(time);
        increaseMonthPostCounter(time);

        return id;
    }

    public void insertPost(ImportedPost post) {
        long postId = insertSimpleRecord(post.source, post.title, post.post, post.time, null);

        for (Category cat : post.categories) {
            long catId = addOrUpdateCategoryByCodename(cat);
            assignCat(catId, postId);
        }

        for (Tag tag : post.tags) {
            long tagId = addOrUpdateTagByCodename(tag);
            assignTag(tagId, postId);
        }
    }

    FourteenDB updatePost(Post post) {
        ContentValues cv = new ContentValues();
        cv.put("source", post.source);
        cv.put("title", post.title);
        cv.put("post", post.post);
        cv.put("time", post.time);
        cv.put("header_img", post.header_image);
        db.update("posts", cv, "_id == " + post.id, null);
        return this;
    }

    long getPostsCount() {
        return DatabaseUtils.queryNumEntries(db, "posts");
    }

    /**
     * First page is 1
     */
    ArrayList<Post> queryPosts(long page) {
        Cursor cursor = db.query(
                "posts", POST_PROJECTION,
                null, null,
                null, null,
                "time DESC limit " + POSTS_PER_PAGE + " offset " + (page - 1) * POSTS_PER_PAGE
        );

        ArrayList<Post> result = new ArrayList<>();

        while (cursor.moveToNext()) {
            result.add(postFromCursor(cursor));
        }

        cursor.close();

        return result;
    }

    Post getPostById(long postId) {

        Cursor cursor = db.query(
                "posts", POST_PROJECTION,
                "_id = " + postId, null,
                null, null, "_id ASC limit 1"
        );

        Post p = null;

        if (cursor.moveToNext()) {
            p = postFromCursor(cursor);
        }

        cursor.close();

        return p;
    }

    FourteenDB deletePost(long postId) {
        db.delete("posts", "_id = " + postId, null);
        recountPosts(); // TODO: just decrease counters
        return this;
    }

    void deletePostsFromSource(long source) {
        db.delete("posts", "source = " + source, null);
    }

    private Post postFromCursor(Cursor cursor) {
        return new Post(blog, editable, cursor.getLong(0),
                cursor.getLong(1), cursor.getString(2), cursor.getString(3), cursor.getLong(4),
                cursor.getLong(5), cursor.getString(6));
    }

    // ======= //
    // AUTHORS //
    // ======= //
    void setAuthor(String codename, String fancyName) {
        ContentValues cv = new ContentValues();
        cv.put("internal_name", codename);
        cv.put("public_name", fancyName);

        if (getPublicAuthorName(1) == null) {
            db.insert("authors", null, cv);
        } else {
            db.update("authors", cv, "_id = 1", null);
        }
    }

    String getPublicAuthorName(long id) {
        Cursor cursor = db.query(
                "authors", new String[]{"public_name"},
                "_id = " + id, null,
                null, null, "_id ASC limit 1"
        );

        if (cursor.moveToNext()) {
            return cursor.getString(0);
        }

        return null;
    }


    // ========== //
    // CATEGORIES //
    // ========== //
    Category[] getAllCategories() {
        ArrayList<Category> result = new ArrayList<>();

        Cursor c = db.query(
                "categories", new String[]{"_id", "codename", "name"},
                null, null,
                null, null, null);

        while (c.moveToNext())
            result.add(new Category(c.getLong(0), c.getString(2), c.getString(1)));

        c.close();

        return result.toArray(new Category[result.size()]);
    }

    Category[] getAssignedCategoriesByPost(long postId) {
        ArrayList<Category> result = new ArrayList<>();

        Cursor cursor = db.query(
                "posts2cats", new String[]{"category"},
                "post = " + postId, null,
                null, null, null
        );

        while (cursor.moveToNext()) {
            Cursor c = db.query(
                    "categories", new String[]{"_id", "codename", "name"},
                    "_id = " + cursor.getLong(0), null,
                    null, null, null);

            if (c.moveToNext())
                result.add(new Category(c.getLong(0), c.getString(2), c.getString(1)));

            c.close();
        }

        cursor.close();

        return result.toArray(new Category[0]);
    }

    Category getCategoryById(long cid) {
        Category result = null;

        Cursor c = db.query(
                "categories", new String[]{"_id", "codename", "name"},
                "_id = " + cid, null,
                null, null, null);

        if (c.moveToNext())
            result = new Category(c.getLong(0), c.getString(2), c.getString(1));

        c.close();

        return result;
    }

    long addOrUpdateCategoryByCodename(Category cat) {
        long id;

        Cursor cursor = db.query(
                "categories", new String[]{"_id"},
                "codename = '" + cat.codename + "'", null,
                null, null, null
        );

        ContentValues cv = new ContentValues();
        cv.put("name", cat.categoryName);

        if (cursor.moveToNext()) {
            id = cursor.getLong(0);

            db.update("categories", cv, "_id = " + id, null);
        } else {
            cv.put("codename", cat.codename);
            id = db.insert("categories", null, cv);
        }

        cursor.close();

        return id;
    }

    private void assignCat(long catId, long postId) {
        ContentValues cv = new ContentValues();
        cv.put("category", catId);
        cv.put("post", postId);
        db.insert("posts2cats", null, cv);
    }

    void reassignCats(long postId, Long[] cats) {
        db.delete("posts2cats", "post = " + postId, null);

        for (Long cat : cats) {
            assignCat(cat, postId);
        }
    }


    // ==== //
    // TAGS //
    // ==== //
    long addOrUpdateTagByCodename(Tag tag) {
        long id;

        Cursor cursor = db.query(
                "tags", new String[]{"_id"},
                "codename = '" + tag.codename + "'", null,
                null, null, null
        );

        ContentValues cv = new ContentValues();
        cv.put("name", tag.nicename);

        if (cursor.moveToNext()) {
            id = cursor.getLong(0);

            db.update("tags", cv, "_id = " + id, null);
        } else {
            cv.put("codename", tag.codename);
            id = db.insert("tags", null, cv);
        }

        cursor.close();

        return id;
    }

    private void assignTag(long tagId, long postId) {
        ContentValues cv = new ContentValues();
        cv.put("tag", tagId);
        cv.put("post", postId);
        db.insert("posts2tags", null, cv);
    }

    Tag[] getAssignedTagsByPost(long postId) {
        ArrayList<Tag> result = new ArrayList<>();

        Cursor cursor = db.query(
                "posts2tags", new String[]{"tag"},
                "post = " + postId, null,
                null, null, null
        );

        while (cursor.moveToNext()) {
            Cursor c = db.query(
                    "tags", new String[]{"_id", "codename", "name"},
                    "_id = " + cursor.getLong(0), null,
                    null, null, null);

            if (c.moveToNext())
                result.add(new Tag(c.getLong(0), c.getString(2), c.getString(1)));

            c.close();
        }

        cursor.close();

        return result.toArray(new Tag[0]);
    }


    // ======= //
    // COUNTER //
    // ======= //
    private void recountPosts() {
        db.delete("postcounter", null, null);


        for (int page = 0; page <= (getPostsCount() / POSTS_PER_PAGE); page++) {

            for (Post post : queryPosts(page + 1)) {
                increaseDayPostCounter(post.time);
                increaseMonthPostCounter(post.time);
            }
        }

    }

    private void increaseDayPostCounter(long time) {
        Date date = new Date(time);
        increasePostCounter(1900 + date.getYear(), date.getMonth() + 1, date.getDate() + 1);
    }

    private void increaseMonthPostCounter(long time) {
        Date date = new Date(time);
        increasePostCounter(1900 + date.getYear(), date.getMonth() + 1, 0);
    }

    private void increasePostCounter(int year, int month, int day) {
        String selection = "year = " + year + " and month = " + month + " and day = " + day;

        Cursor cursor = db.query("postcounter", new String[]{"cnt"}, selection,
                null, null, null, null);


        if (cursor.moveToNext()) {
            int cnt = cursor.getInt(0);
            cnt++;

            ContentValues cv = new ContentValues();
            cv.put("cnt", cnt);
            db.update("postcounter", cv, selection, null);
        } else {

            ContentValues cv = new ContentValues();
            cv.put("year", year);
            cv.put("month", month);
            cv.put("day", day);
            cv.put("cnt", 1);

            db.insert("postcounter", null, cv);
        }

        cursor.close();
    }

    int[] getYearsWithPosts() {
        // TODO: lookup values in "postcounter" table
        return new int[]{
                1970,
                2014,
                2015,
                2016,
                2017,
                2018,
                2019,
                2020,
                2021,
                2022,
                2023
        };
    }

    int[] getPostsCountByYear(int year) {
        int[] result = new int[12];

        Cursor c = db.query("postcounter", new String[]{"month", "cnt"},
                "year = " + year + " and day = 0",
                null, null, null, "month");

        while (c.moveToNext()) {
            result[c.getInt(0) - 1] = c.getInt(1);
        }

        c.close();

        return result;
    }

    /**
     * Get posts counts per-day by given year and month
     *
     * @param year  ...
     * @param month ...
     * @return integer array where element at position i representing post count at date (i-1) of
     * requested month. Returned array always 33 elements long.
     */
    int[] getPostsCountByMonth(int year, int month) {
        Cursor c = db.query("postcounter", new String[]{"day", "cnt"},
                "year = " + year + " and month = " + month,
                null, null, null, "day");

        // Примечательно, что по какой-то причине (известно какой - по причине программиста)
        // дата смещена на один день вперед. Поэтому 31-е число имеет индекс 32. Соответственно
        // массив состоит из 33-х элементов
        int[] result = new int[33];

        while (c.moveToNext()) {
            int day = c.getInt(0);
            int count = c.getInt(1);
            result[day] = count;
        }

        c.close();

        return result;
    }


    // ======= //
    // SOURCES //
    // ======= //
    public long insertSource(String name) {
        ContentValues cv = new ContentValues();
        cv.put("name", name);

        return db.insert("sources", null, cv);
    }

    public void updateSource(Source source) {
        ContentValues cv = new ContentValues();
        cv.put("name", source.name);
        db.update("sources", cv, "_id = " + source.id, null);
    }

    public void deleteSource(long id) {
        db.delete("sources", "_id = " + id, null);
    }

    public String getSourceName(long id) {
        if (id < 0) {
            return "Corrupted source";
        } else if (id == 0) {
            return "Own notes";
        } else {
            String s = "[Internal error]";
            Cursor c = db.query("sources", new String[]{"name"},
                    "_id = '" + id + "'", null, null, null, null);

            if (c.moveToNext())
                s = c.getString(0);

            c.close();

            return s;
        }
    }

    public Source[] getAllSources() {

        ArrayList<Source> list = new ArrayList<>();

        Cursor c = db.query("sources", new String[]{"_id", "name"},
                null, null, null, null, null);

        while (c.moveToNext())
            list.add(new Source(c.getLong(0), c.getString(1)));

        c.close();

        return list.toArray(new Source[0]);
    }


    public static class OpenHelper extends SQLiteOpenHelper {

        private static final int version = 6;

        private OpenHelper(final Context context, final String name) {
            super(new MyContextWrapper(context), name, null, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }

    static class Setting {
        static final String BLOG_NAME = "blog_name";
    }

    static class MyContextWrapper extends ContextWrapper {

        Context context;

        public MyContextWrapper(Context base) {
            super(base);
            this.context = base;
        }

        @Override
        public File getDatabasePath(String name) {
            File file = new File(Const.EXT_ROOT, name + "/main.db");

            if (!file.exists()) {
                try {
                    file.getParentFile().mkdirs();
                    file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return file;
        }

        /* this version is called for android devices >= api-11. thank to @damccull for fixing this. */
        @Override
        public SQLiteDatabase openOrCreateDatabase(String name, int mode, SQLiteDatabase.CursorFactory factory, DatabaseErrorHandler errorHandler) {
            return openOrCreateDatabase(name, mode, factory);
        }

        /* this version is called for android devices < api-11 */
        @Override
        public SQLiteDatabase openOrCreateDatabase(String name, int mode, SQLiteDatabase.CursorFactory factory) {
            return SQLiteDatabase.openOrCreateDatabase(getDatabasePath(name), null);
        }
    }
}
