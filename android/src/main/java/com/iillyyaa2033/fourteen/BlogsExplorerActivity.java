package com.iillyyaa2033.fourteen;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.iillyyaa2033.fourteen.model.Methods;

import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class BlogsExplorerActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ListView list = new ListView(this);
        setContentView(list);

        List<String> names = new ArrayList<>();
        File root = new File(Const.EXT_ROOT);
        if(root.exists()){
            names.add(getString(R.string.local_blog));
            for(File f : root.listFiles()){
                if(f.isDirectory() && !f.getName().equals(Const.LOCAL_BLOG)){
                    names.add(f.getName());
                }
            }
        }

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, names);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String name = adapter.getItem(i);
                if(name.equals(getString(R.string.local_blog))) name = Const.LOCAL_BLOG;

                URI uri = Utils.getUri(name, Methods.VIEW_BLOG, 1);

                startActivity(new Intent(BlogsExplorerActivity.this, MainActivity.class).setData(Uri.parse(uri.toString())));
            }
        });
    }
}
