package com.iillyyaa2033.fourteen;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;

import java.util.ArrayList;

public class ParallaxView extends View {

    private float limit = 1;
    private float last = 0;

    private Paint paint = new Paint();

    private ArrayList<Layer> layers = new ArrayList<>();

    public ParallaxView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ParallaxView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init(){
        paint.setColor(Color.WHITE);
    }

    public void destroy(){
        for(Layer l : layers){
            l.bm.recycle();
        }
    }

    public void bindScrollView(final ScrollView scroll){

        scroll.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                limit = scroll.getChildAt(0).getHeight();
                limit = limit > 0 ? limit : 1;
                last = scroll.getScrollY();
                invalidate();
            }
        });
    }

    public void addTiledLayer(int drawable, float coef){
        Bitmap bm = BitmapFactory.decodeResource(getResources(), drawable);
        layers.add(new Layer(bm, coef, Layer.MODE_TILED));
    }

    public void addScaledLayer(int drawable){
        Bitmap bm = BitmapFactory.decodeResource(getResources(), drawable);
        layers.add(new Layer(bm, 1f, Layer.MODE_SCALED));
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);

        canvas.drawRect(0,0,getWidth(),getHeight(), paint);

        for(Layer layer : layers){
            drawLayer(canvas, layer);
        }
    }

    private void drawLayer(Canvas canvas, Layer layer){
        switch (layer.mode){
            case Layer.MODE_TILED:
                drawTiledLayer(canvas, layer);
                break;
            case Layer.MODE_SCALED:
                drawScaledLayer(canvas, layer);
                break;
        }
    }

    private void drawTiledLayer(Canvas canvas, Layer layer){
        float sidegap = (layer.bm.getWidth() - getWidth()) / 2f;
        sidegap = sidegap > 0 ? sidegap : 0;

        float heightByWidth = 1f * getHeight() / getWidth();
        float height = (layer.bm.getWidth() - 2f*sidegap) * heightByWidth;
        float offset = (last * layer.coef) % layer.bm.getHeight();

        Rect src = new Rect((int)sidegap, (int) offset, layer.bm.getWidth() - (int) sidegap, (int) (offset + height));
        Rect dst = new Rect(0, 0, getWidth(), getHeight());

        if(src.bottom < layer.bm.getHeight()) {
            canvas.drawBitmap(layer.bm, src, dst, paint);
        } else {
            int overheight = (int) ((offset + height) - layer.bm.getHeight());

            src.bottom = layer.bm.getHeight();
            dst.bottom = getHeight() - overheight;
            canvas.drawBitmap(layer.bm, src, dst, paint);

            src.top = 0;
            src.bottom = overheight;
            dst.top = getHeight() - overheight;
            dst.bottom = getHeight();
            canvas.drawBitmap(layer.bm, src, dst, paint);
        }
    }

    private void drawScaledLayer(Canvas canvas, Layer layer) {
        float sidegap = (layer.bm.getWidth() - getWidth()) / 2f;
        sidegap = sidegap > 0 ? sidegap : 0;

        float offset = (last / limit) * (layer.bm.getHeight() - getHeight());

        Rect src = new Rect((int) sidegap, (int) offset, layer.bm.getWidth() - (int) sidegap, (int) (offset + getHeight()));
        Rect dst = new Rect(0, 0, getWidth(), getHeight());

        canvas.drawBitmap(layer.bm, src, dst, paint);
    }

    static class Layer{

        static final int MODE_TILED = 0;
        static final int MODE_SCALED = 1;

        int mode;
        Bitmap bm;
        float coef;

        Layer(Bitmap bm, float coef, int mode){
            this.bm = bm;
            this.coef = coef;
            this.mode = mode;
        }

    }
}
