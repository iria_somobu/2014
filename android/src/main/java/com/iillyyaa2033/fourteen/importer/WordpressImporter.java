package com.iillyyaa2033.fourteen.importer;

import com.iillyyaa2033.fourteen.Const;
import com.iillyyaa2033.fourteen.FourteenDB;
import com.iillyyaa2033.fourteen.R;
import com.iillyyaa2033.fourteen.model.Category;
import com.iillyyaa2033.fourteen.model.ImportedPost;
import com.iillyyaa2033.fourteen.model.Tag;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class WordpressImporter extends DefaultHandler implements IImporter {

    private ImporterActivity activity;

    private ArrayList<ImportedPost> posts = new ArrayList<>();
    private long postsCounter = 0;
    private ImportedPost currentPost = null;

    private String lastNode = "";

    WordpressImporter(ImporterActivity activity){
        this.activity = activity;
    }

    public void parse(InputStream is) throws IOException, SAXException, ParserConfigurationException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();

        parser.parse(is, this);
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        lastNode = qName;

        switch(qName) {
            case "item":
                currentPost = new ImportedPost();
                break;
            case "category":
                if(currentPost != null){
                    String type = attributes.getValue("domain");
                    String nicename = URLDecoder.decode(attributes.getValue("nicename"));

                    switch (type) {
                        case "category":
                            Category c = new Category(nicename, nicename);
                            currentPost.categories.add(c);
                            break;
                        case "post_tag":
                            Tag tag = new Tag(nicename, nicename);
                            currentPost.tags.add(tag);
                            break;
                    }
                }
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {

        String text = new String(ch, start, length).replace("\n", "").trim();

        if(!text.isEmpty() && currentPost != null) {
            switch(lastNode) {
                case "title":
                    currentPost.title += " "+text;
                    currentPost.title = currentPost.title.trim();
                    break;
                case "pubDate":
                    try {
                        DateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);
                        Date date = formatter.parse(text);
                        currentPost.time = date.getTime();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    break;
                case "content:encoded":
                    currentPost.post += " "+ text.replace("<![CDATA[", "").replace("]]>", "");
                    currentPost.post = currentPost.post.trim();
                    break;
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

        switch(qName) {
            case "item":
                posts.add(currentPost);
                postsCounter++;
                activity.publishProgress(activity.getString(R.string.importer_imp)+"\n"+postsCounter);
                currentPost = null;
                break;
        }
    }

    @Override
    public void endDocument() throws SAXException {

    }

    @Override
    public boolean isSupported(String mime) {
        return "text/xml".equals(mime);
    }

    @Override
    public String getLabel(String mime) {
        return activity.getString(R.string.import_as_wxr);
    }

    @Override
    public void startImport(String extension, InputStream is) {
        try {
            parse(is);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveResults() {
        activity.publishProgress(R.string.importer_ins_db);

        int i = 0;

        try {
            FourteenDB db = FourteenDB.open(activity, Const.LOCAL_BLOG);

            long source = db.insertSource(
                    activity.getString(
                            R.string.importer_source_file,
                            SimpleDateFormat.getDateTimeInstance().format(new Date())));

            for (ImportedPost post : posts) {
                post.source = source;
                db.insertPost(post);
                i++;
                activity.publishProgress(activity.getString(R.string.importer_ins_db) + "\n" + i + "/" + posts.size());
            }
            db.close();
        } catch (Exception e){
            e.printStackTrace();
            System.out.println(posts.get(i));
        }
    }
}
