package com.iillyyaa2033.fourteen.importer;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iillyyaa2033.fourteen.R;

import java.io.InputStream;

public class ImporterActivity extends Activity {

    Handler handler;
    TextView text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_importer);

        handler = new Handler();

        TextView type = findViewById(R.id.detected);
        type.setText(getString(R.string.importer_type_detected)+" "+getIntent().getType());

        makeButtons();

        text = findViewById(R.id.step);
    }

    private void makeButtons(){
        IImporter[] importers = {
                new WordpressImporter(this),
                new ImageImporter(this)
        };

        String mimetype = getIntent().getType();

        if(mimetype == null) return;

        for(final IImporter importer : importers){
            if(importer.isSupported(mimetype)){
                Button button = new Button(this);
                button.setText(importer.getLabel(mimetype));
                button.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        startImportProcess(importer);
                    }
                });

                ((LinearLayout) findViewById(R.id.buttons_root)).addView(button);
            }
        }
    }

    public void publishProgress(final int text){
        publishProgress(getString(text));
    }

    public void publishProgress(final String text){
        handler.post(new Runnable() {
            @Override
            public void run() {
                ImporterActivity.this.text.setText(text);
            }
        });
    }

    private void startImportProcess(final IImporter importer){
        findViewById(R.id.buttons_root).setVisibility(View.GONE);
        findViewById(R.id.progress_root).setVisibility(View.VISIBLE);


        new Thread(){

            @Override
            public void run() {

                try {
                    publishProgress(R.string.importer_starting);

                    InputStream is = getContentResolver().openInputStream((Uri) getIntent().getExtras().get(Intent.EXTRA_STREAM));

                    publishProgress(R.string.importer_imp);
                    String mime = getIntent().getType();
                    importer.startImport(mime.substring(mime.indexOf("/")+1), is);
                    is.close();

                    importer.saveResults();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                finishImporting();
            }
        }.start();
    }

    private void finishImporting(){
        handler.post(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        });
    }
}
