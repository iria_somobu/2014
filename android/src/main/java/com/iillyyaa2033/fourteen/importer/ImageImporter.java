package com.iillyyaa2033.fourteen.importer;

import com.iillyyaa2033.fourteen.Const;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;

public class ImageImporter implements IImporter {

    ImporterActivity activity;

    ImageImporter(ImporterActivity activity){
        this.activity = activity;
    }

    @Override
    public boolean isSupported(String mime) {
        return mime.startsWith("image/");
    }

    @Override
    public String getLabel(String mime) {
        return "Import as image";
    }

    @Override
    public void startImport(String extension, InputStream is) {
        Calendar calendar = Calendar.getInstance();

        String path = String.format("%d/%02d/%02d/%02d-%02d-%02d-%03d."+extension,
                calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH),
                calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND), calendar.get(Calendar.MILLISECOND));

        try {

            File file = new File(Const.EXT_ROOT, "/"+ Const.LOCAL_BLOG +"/"+path);
            file.getParentFile().mkdirs();
            file.createNewFile();
            OutputStream outputStream = new FileOutputStream(file);

            byte[] buffer = new byte[1024];
            while(is.read(buffer) != -1)
                outputStream.write(buffer);

            outputStream.close();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void saveResults() {

    }
}
