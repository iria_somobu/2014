package com.iillyyaa2033.fourteen.importer;

import java.io.InputStream;

public interface IImporter {

    boolean isSupported(String mime);
    String getLabel(String mime);
    void startImport(String extension, InputStream is);
    void saveResults();
}
