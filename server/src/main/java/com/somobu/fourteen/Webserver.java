package com.somobu.fourteen;

import com.iillyyaa2033.fourteen.Const;
import com.iillyyaa2033.fourteen.Utils;
import com.iillyyaa2033.fourteen.api.Post;
import com.iillyyaa2033.fourteen.model.Category;
import com.iillyyaa2033.fourteen.model.Methods;
import com.iillyyaa2033.fourteen.routing.Page;
import com.iillyyaa2033.fourteen.routing.PostsListPage;
import com.iillyyaa2033.fourteen.routing.Router;
import com.iillyyaa2033.fourteen.routing.SinglePostPage;
import fi.iki.elonen.NanoHTTPD;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Webserver extends NanoHTTPD {

    final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm dd.MM.yyyy z");

    public Webserver(int port) {
        super(port);
    }

    @Override
    public Response serve(IHTTPSession session) {
        String uri = session.getUri();
        String qps = session.getQueryParameterString();
        if (qps != null) uri += qps;

        try {
            switch (uri) {
                case "/style.css":
                    return getStylesheet();
                case "/favicon.ico":
                    return getIcon();
                default:
                    return route(uri);
            }
        } catch (Exception e) {
            return getError(e);
        }
    }

    Response getStylesheet() throws IOException {
        String style = readString("web/style.css");
        return newFixedLengthResponse(Response.Status.OK, "text/css", style);
    }

    Response getIcon() throws IOException {
        FileInputStream fis = new FileInputStream(new File("web/favicon.ico"));
        return newChunkedResponse(Response.Status.OK, "image/x-icon", fis);
    }

    Response route(String uri) throws IOException {
        if ("/".equals(uri)) uri = Utils.getUri(Const.LOCAL_BLOG, Methods.VIEW_BLOG, 1).toString();
        else uri = "fourteen://" + Const.LOCAL_BLOG + uri;

        System.out.println("Routing " + uri);

        Page page = Router.route(URI.create(uri));

        if (page instanceof PostsListPage) {
            return getMainPage((PostsListPage) page);
        } else if (page instanceof SinglePostPage) {
            return getUnimplemented();
        } else {
            return get404();
        }
    }

    Response getMainPage(PostsListPage page) throws IOException {
        StringBuilder s = new StringBuilder(formatHeader(page.blog_name));

//        for (Post p : page.posts) s.append(formatSinglePost(p, p.cats));

        s.append(formatFooter());

        return newFixedLengthResponse(s.toString());
    }

    Response get404() {
        return newFixedLengthResponse(Response.Status.NOT_FOUND, "text/plain", "Not found");
    }

    Response getUnimplemented() {
        return newFixedLengthResponse(Response.Status.NOT_FOUND, "text/plain", "Unimplemented");
    }

    Response getError(Exception e) {
        String s = "Internal error!\n\n";

        Throwable ep = e;
        String prefix = "\n";
        do {
            s += prefix + e.getClass() + " " + e.getMessage();
            prefix = prefix + "  ";

            for (StackTraceElement el : ep.getStackTrace()) {
                s += prefix + el;
            }

            ep = ep.getCause();
        } while (ep != null);

        return newFixedLengthResponse(Response.Status.INTERNAL_ERROR, "text/plain", s);
    }

    String formatHeader(String blogname) throws IOException {
        return new Builder("web/header.txt")
                .pair("{TITLE}", blogname)
                .pair("{SUBTITLE}", "Do we have a city to burn?")
                .result();
    }

    String formatFooter() throws IOException {
        return readString("web/footer.txt");
    }

    String formatSinglePost(Post p, Category[] cats) throws IOException {
        Builder b = new Builder("web/post.txt");

        b.pair("{TITLE}", p.title);
        b.pair("{TITLE_URL}", Methods.VIEW_POST + "#" + p.id);
        b.pair("{AUTHOR}", "" + p.author_pubname);
        b.pair("{AUTHOR_URL}", "#");
        b.pair("{TIME}", sdf.format(new Date(p.time)));
        b.pair("{CONTENT}", p.post);

        if (cats.length == 0) {
            b.pair("{CATS}", "this blog");
        } else {
            String[] strCats = new String[cats.length];
            for (int i = 0; i < cats.length; i++) strCats[i] = "<a href=\"#\">" + cats[i].categoryName + "</a>";
            b.pair("{CATS}", String.join(", ", strCats));
        }

        return b.result();
    }

    static String readString(String path) throws IOException {
        return Files.readString(new File(path).toPath(), StandardCharsets.UTF_8);
    }

    static class Builder {

        private String s;

        public Builder(String path) throws IOException {
            s = readString(path);
        }

        public Builder pair(String key, String value) {
            if (value == null) value = "EMPTY";
            s = s.replace(key, value);
            return this;
        }

        public String result() {
            return s;
        }
    }
}
