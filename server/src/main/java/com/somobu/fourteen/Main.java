package com.somobu.fourteen;

import com.iillyyaa2033.fourteen.*;
import com.iillyyaa2033.fourteen.model.Methods;
import com.iillyyaa2033.fourteen.routing.Router;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.sql.SQLException;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException, SQLException {
        Platform.setImplementation(new IPlatform() {
            @Override
            public String getJdbcPath(String name) {
                return "jdbc:sqlite:"+name+"/main.db";
            }
        });

        new Main().run();
    }

    synchronized void run() throws IOException, InterruptedException, SQLException {
        Webserver server = new Webserver(4221);
        server.start();

        // Syncthing creates '.syncthing.main.db.tmp' file, when renames it to 'main.db'
        // So we should watch to ENTRY_MODIFY event of '.syncthing.main.db.tmp'
        // and then reload our database connection.
        /// Technically, this is a hack that must be replaced with proper solution. But I cannot find one

        WatchService ws = FileSystems.getDefault().newWatchService();
        Path path = Paths.get(new File(Const.LOCAL_BLOG).getAbsolutePath());
        path.register(ws, StandardWatchEventKinds.ENTRY_MODIFY);

        WatchKey key;
        while ((key = ws.take()) != null) {

            boolean reload = false;
            for (WatchEvent<?> e : key.pollEvents()) {
                String context = e.context().toString();
                if (context.equals(".syncthing.main.db.tmp")) {
                    reload = true;
                }
            }
            if (!reload) continue;

            Platform.closeDb(Const.LOCAL_BLOG);

            key.reset();
        }
    }

}
