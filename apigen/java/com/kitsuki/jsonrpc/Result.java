package com.kitsuki.jsonrpc;

class Result {

    long rqCode;
    Object result;
    Exception error;

    Result(long code, Object result, Exception error) {
        this.rqCode = code;
        this.result = result;
        this.error = error;
    }

}
