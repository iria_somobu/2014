package com.kitsuki.jsonrpc;

import com.google.gson.*;
import com.iillyyaa2033.utils.ThreadingUtils;
import org.newsclub.net.unix.AFUNIXSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class UnixSocketPeer extends Thread {

    private static final Gson gson = new GsonBuilder()
            .addSerializationExclusionStrategy(new H2WorkaroundExclusionStrategy())
            .addDeserializationExclusionStrategy(new H2WorkaroundExclusionStrategy())
            .create();
    private final Logger log = LoggerFactory.getLogger(UnixSocketPeer.class);

    private boolean isServer;

    private static final Executor executor = Executors.newCachedThreadPool();
    private final List<PendingRequest> pendingRequests = Collections.synchronizedList(new ArrayList<>());
    private Object localMethods;
    private Object remoteInterface;

    private AFUNIXSocket socket;
    private InputStream is;
    private OutputStream os;

    private DisconnectListener l;

    public void start(boolean isServer, AFUNIXSocket sock, Object localMethods) throws IOException {
        this.isServer = isServer;
        this.socket = sock;
        this.localMethods = localMethods;
        is = sock.getInputStream();
        os = sock.getOutputStream();
        start();
    }

    public void close() throws IOException {
        interrupt();
        socket.close();
    }

    public void setL(DisconnectListener l) {
        this.l = l;
    }

    public Object getRemoteInterface() {
        return remoteInterface;
    }

    public synchronized Object getRemoteInterface(boolean isServerFromHere, Class<?> targetClass) {
        if (remoteInterface == null) remoteInterface = newRemoteInterface(isServerFromHere, targetClass);
        return remoteInterface;
    }

    private Object newRemoteInterface(boolean isServerFromHere, Class<?> targetClass) {
        ClassLoader cl = targetClass.getClassLoader();
        Class<?>[] classes = new Class[]{targetClass};

        return Proxy.newProxyInstance(cl, classes, new InvocationHandler() {

            private long rqCounter = isServerFromHere ? -1 : 1;

            @Override
            public Object invoke(Object o, Method method, Object[] objects) throws Throwable {

                PendingRequest rq = new PendingRequest(rqCounter, method.getReturnType());
                synchronized (pendingRequests) {
                    pendingRequests.add(rq);
                }

                if (isServer) rqCounter--;
                else rqCounter++;

                writeMessage(new Request(rq.rqNum, method.getName(), objects));

                // Awaiting result
                Result result = rq.waitResult();
                synchronized (pendingRequests) {
                    pendingRequests.remove(rq);
                }

                if (result.error == null) {
                    return result.result;
                } else {
                    throw result.error;
                }
            }
        });
    }

    @Override
    public void run() {
        try {
            java.util.Scanner s = new java.util.Scanner(is);
            while (!isInterrupted() && s.hasNextLine()) {
                String line = s.nextLine();
                log.trace("Message read: " + line);

                JsonObject json = (JsonObject) JsonParser.parseString(line);
                long rqNum = json.getAsJsonPrimitive("rqCode").getAsLong();
                boolean isRequest = (isServer && rqNum > 0) || (!isServer && rqNum < 0);

                if (isRequest) {
                    executor.execute(() -> {
                        Object o = null;
                        Exception e = null;

                        try {
                            o = execOnLocal(json);
                        } catch (Exception ex) {
                            e = ex;
                            log.error("Caught error while execOnLocal", e);
                        } finally {
                            try {
                                writeMessage(new Result(rqNum, o, e));
                            } catch (IOException ioException) {
                                log.error("Unable to write message", ioException);
                            }
                        }
                    });

                } else {
                    synchronized (pendingRequests) {
                        for (PendingRequest rq : pendingRequests) {
                            if (rq.rqNum == rqNum) {
                                try {
                                    Object resultData = gson.fromJson(json.get("result"), rq.getResultType());

                                    Exception resultError = null;
                                    if (json.has("error")) {
                                        resultError = gson.fromJson(json.getAsJsonObject("error"), Exception.class);
                                    }

                                    rq.postResult(new Result(rqNum, resultData, resultError));
                                } catch (Exception e) {
                                    log.error("Caught error on pending request deserialization", e);
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            log.error("", e);
        } finally {
            try {
                if (l != null) l.onDisconnected(this);
                if (is != null) is.close();
                if (os != null) os.close();
            } catch (IOException e) {
                log.error("", e);
            }
        }
    }

    private Object execOnLocal(JsonObject request) throws InvocationTargetException, IllegalAccessException {
        JsonArray rawArgs = request.getAsJsonArray("args");
        String methodName = request.get("methodName").getAsString();
        int argcnt = rawArgs == null ? 0 : rawArgs.size();

        Method m = null;
        Object[] args = null;
        for (Method me : localMethods.getClass().getMethods()) {
            if (me.getName().equals(methodName)) {
                Class<?>[] params = me.getParameterTypes();
                if (params.length == argcnt) {
                    m = me;

                    args = new Object[argcnt];
                    for (int i = 0; i < params.length; i++) {
                        args[i] = gson.fromJson(rawArgs.get(i), params[i]);
                    }

                    break;
                }
            }
        }

        if (m == null)
            throw new RuntimeException("Unable to find method " + methodName + " on " + localMethods.getClass());

        m.setAccessible(true);
        return m.invoke(localMethods, args);
    }

    private void writeMessage(Object rs) throws IOException {
        String json = gson.toJson(rs);
        String r = json + "\n";
        os.write(r.getBytes());
        os.flush();
        log.trace("Message written: " + json);
    }

    private static class PendingRequest {

        public final long rqNum;
        private final ThreadingUtils.MyLock lock = new ThreadingUtils.MyLock();
        private final Class<?> clazz;
        private volatile Result result = null;

        PendingRequest(long rqNum, Class<?> clazz) {
            this.rqNum = rqNum;
            this.clazz = clazz;
            lock.lock();
        }

        public Result waitResult() {
            lock.waitUnlock();

            return result;
        }

        public void postResult(Result result) {
            this.result = result;
            lock.unlock();
        }

        public Type getResultType() {
            return clazz;
        }

    }

    interface DisconnectListener {

        void onDisconnected(UnixSocketPeer peeer);

    }
}
