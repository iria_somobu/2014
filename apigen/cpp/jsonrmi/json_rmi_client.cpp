#include <zconf.h>
#include <value.h>
#include <condition_variable>

#include "json_rmi_client.hpp"

struct JsonRpcRequest {
    std::mutex mutex_;
    std::condition_variable var;

    long id;
    Json::Value json;

    explicit JsonRpcRequest(long _id);

    void wait_till_job_is_done();

    void release_var(Json::Value& _json);
};

JsonRpcRequest::JsonRpcRequest(long _id) {
    id = _id;
}

void JsonRpcRequest::wait_till_job_is_done() {
    std::unique_lock<std::mutex> lck(mutex_);
    var.wait(lck);
}

void JsonRpcRequest::release_var(Json::Value& _json) {
    json = _json;
    var.notify_all();
}

JsonRpcClient::JsonRpcClient(const std::string &socket_path) {
    if ((fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        perror("socket error");
        exit(-1);
        // TODO: replace with proper error (exception?)
    }

    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strncpy(addr.sun_path, socket_path.c_str(), socket_path.length());

    builder["indentation"] = "";
    builder["dropNullPlaceholders"] = true;
    builder["emitUTF8"] = true;
}


void JsonRpcClient::start() {

    if (connect(fd, (struct sockaddr *) &addr, sizeof(addr)) == -1) {
        perror("connect error");
        exit(-1);
    } else {
        reader_thread = new std::thread([&] {
            while (true){
                // Inefficient code below:
                std::string rsStr;
                char buf[1024];
                while (true) {
                    int rc = read(fd, buf, sizeof(buf));

                    if (rc <= 0) return;
                    rsStr.append(std::string(buf, rc));
                    if (buf[rc - 1] == '\n') break;
                }
                Json::Value rsRoot;
                reader.parse(rsStr, rsRoot);

                int my_id = rsRoot["rqCode"].asInt();

                fcn_call_mtx.lock();
                for (unsigned long i = 0; i < requests.size(); i++) {
                    if (requests[i]->id == my_id) {
                        requests[i]->release_var(rsRoot);
                        requests.erase(requests.begin() + i);
                    }
                }
                fcn_call_mtx.unlock();
            }
        });
    }
}


void JsonRpcClient::stop() {
    shutdown(fd, SHUT_RDWR);
}


void JsonRpcClient::setCallback(void *callback) {
    // TODO: implement me
}


Json::Value JsonRpcClient::call(const std::string &method, Json::Value &args) {
    if (args.type() != Json::arrayValue) throw std::runtime_error("'args' must be json array");

    Json::Value rqRoot;
    rqRoot["methodName"] = method;
    rqRoot["args"] = args;

    // Сча будем обновлять переменные и записывать в фаил -- блочимся
    fcn_call_mtx.lock();

    methodIdCounter++;
    int my_id = methodIdCounter;
    JsonRpcRequest rq(my_id);
    requests.push_back(&rq);

    rqRoot["rqCode"] = my_id;
    std::string rqStr = Json::writeString(builder, rqRoot) + "\n";
    write(fd, rqStr.c_str(), rqStr.length());

    fcn_call_mtx.unlock(); // Отправили запрос, отдали доступ к памяти

    rq.wait_till_job_is_done(); // Ждем, пока придет ответ

    return rq.json["result"];
}