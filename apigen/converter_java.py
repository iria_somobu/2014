#!/usr/bin/python3
import os
import converter_abs as conv

verbose = False

src = "../api.md"
dst = '../common/src/main/java/com/iillyyaa2033/fourteen/api/'

packageName = 'com.iillyyaa2033.fourteen.api'
eventsFileName = 'IEvents'
methodsFileName = 'IMethods'

jClassMappings = {
    "str": "String",
    "bool": "boolean"
}


def get_full_type(type, flags):
    arrayMark = ''
    if conv.FIELD_FLAG_ARRAY in flags:
        arrayMark = "[]"

    return jClassMappings.get(type, type) + arrayMark


def write_intf(path, name, objs):
    f = open(path, 'w+')

    f.write('package '+packageName+';\n')
    f.write('\n')
    f.write("public interface "+name+" {\n")

    for obj in objs:

        return_type = "void"

        f.write('\n  /**\n')
        for dc in obj.descr.split('\n'):
            dc = dc.strip(' ')
            if len(dc) != 0:
                f.write("   * " + dc + '\n')

        f.write('   * \n')

        for i in range(len(obj.field_names)):
            if obj.field_names[i] == "@return":
                return_type = get_full_type(obj.field_types[i], obj.field_flags[i])
                f.write('   * '+obj.field_names[i] + ' '+obj.field_descs[i] + '\n')
            else:
                f.write('   * @param ' + obj.field_names[i] + ' '+obj.field_descs[i] + '\n')
        f.write('   */\n')

        f.write('  '+return_type + " "+obj.name + "(")

        for i in range(len(obj.field_names)):
            if obj.field_names[i] != "@return":
                if i > 0:
                    f.write(', ')

                type = get_full_type(obj.field_types[i], obj.field_flags[i])
                f.write(type + ' ' + obj.field_names[i])

        f.write(');\n')

    f.write("}\n")


# ============= #
# Run converter #
# ============= #

# TODO: parse cmdline args

if verbose: print("Generating output...")
conv.parse_file(src)

if verbose: print("Writing into folder", dst)
if not os.path.exists(dst):
    os.makedirs(dst)

for obj in conv.objects:
    f = open(dst + obj.name + ".java", 'w+')

    f.write('package '+packageName+';\n')
    f.write('\n')
    f.write('/**\n')
    for dc in obj.descr.split('\n'):
        dc = dc.strip(' ')
        if len(dc) != 0:
            f.write(" * " + dc + '\n')
    f.write(' */\n')

    f.write('public class ' + obj.name + ' {\n')

    for i in range(len(obj.field_names)):
        f.write('\n')
        f.write('  /**\n')
        for dc in obj.field_descs[i].split('\n'):
            dc = dc.strip(' ')
            if len(dc) != 0:
                f.write("   * " + dc + '\n')
        f.write('   */\n')

        mappedType = get_full_type(obj.field_types[i], obj.field_flags[i])
        f.write('  public ' + mappedType + ' ' + obj.field_names[i] + ';\n')

    f.write('\n}\n')

write_intf(dst + "/" + methodsFileName + ".java", methodsFileName, conv.methods)
write_intf(dst + "/" + eventsFileName + ".java", eventsFileName, conv.events)


if verbose: print("Done!")
