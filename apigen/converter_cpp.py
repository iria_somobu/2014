#!/usr/bin/python3
import os
import converter_abs as conv

src = "../api.md"
dst = '../curses/api/'

cppClassMappings = {
    "bool": "bool",
    "int": "int",
    "long": "long",
    "str": "std::string"
}
jsonConvMappings = {
    "bool": "asBool()",
    "int": "asInt()",
    "long": "asInt64()",
    "str": "asString()"
}


def get_full_type(type, flags):
    array_mark_prefix = ''
    array_mark_suffix = ''

    if conv.FIELD_FLAG_ARRAY in flags:
        array_mark_prefix = "std::vector<"
        array_mark_suffix = ">"

    map_type = cppClassMappings.get(type, "std::shared_ptr<" + type + ">")
    return array_mark_prefix + map_type + array_mark_suffix


def write_model_header(folder):
    f = open(folder + "model.hpp", 'w+')

    f.write('#include <string>\n')
    f.write('#include <vector>\n')
    f.write('#include <json.h>\n')
    f.write('\n')
    f.write('#pragma once\n')
    f.write('\n')

    # Generate declarations
    for obj in conv.objects:
        f.write('struct ' + obj.name + ';\n')
    f.write('\n')

    # Actual class descriptions
    for obj in conv.objects:
        f.write('/**\n')
        for dc in obj.descr.split('\n'):
            dc = dc.strip(' ')
            if len(dc) != 0:
                f.write(" * " + dc + '\n')
        f.write(' */\n')

        f.write('struct ' + obj.name + ' {\n')

        for i in range(len(obj.field_names)):
            f.write('\n')
            f.write('  /**\n')
            for dc in obj.field_descs[i].split('\n'):
                dc = dc.strip(' ')
                if len(dc) != 0:
                    f.write("   * " + dc + '\n')
            f.write('   */\n')

            mappedType = get_full_type(obj.field_types[i], obj.field_flags[i])

            f.write('  ' + mappedType + ' ' + obj.field_names[i] + ';\n')

        # Json IO method
        f.write('\n  Json::Value to_json();\n')
        f.write('\n  static std::shared_ptr<'+obj.name + '> from_json(Json::Value& json);\n')

        f.write('\n};\n\n\n')


def write_model_body(folder):
    f = open(folder + "model.cpp", 'w+')

    pd = "  "

    f.write('#include "model.hpp"\n')
    f.write('\n')
    for obj in conv.objects:

        # Serialization
        f.write('\nJson::Value '+obj.name+'::to_json(){\n')
        f.write(pd+'Json::Value json;\n')

        for i in range(len(obj.field_names)):
            n = obj.field_names[i]
            proper_suffix = "" if (obj.field_types[i] in cppClassMappings) else "->to_json()"
            if conv.FIELD_FLAG_ARRAY in obj.field_flags[i]:
                f.write(pd+'{\n')
                f.write(pd+pd+'Json::Value val;\n')
                f.write(pd+pd+'for(int i = 0; i < '+n+'.size(); i++) val[i] = '+n+'[i]'+proper_suffix+';\n')
                f.write(pd+pd+'json["'+n+'"] = val;\n')
                f.write(pd+'}\n')
            else:
                f.write(pd+'json["'+n+'"] = ' + n + proper_suffix + ';\n')

        f.write(pd+'return json;\n')
        f.write('}\n\n')

        # Deserialization
        f.write('\nstd::shared_ptr<'+obj.name + '> '+obj.name+'::from_json(Json::Value& json){\n')
        f.write(pd+obj.name+' *result = new '+obj.name+'();\n')

        for i in range(len(obj.field_names)):
            n = obj.field_names[i]
            rt = obj.field_types[i]
            jsn = 'json["'+n+'"]'

            if conv.FIELD_FLAG_ARRAY in obj.field_flags[i]:
                pass
                f.write(pd+'{\n')
                ft = get_full_type(obj.field_types[i], obj.field_flags[i])
                cv = jsn+"[i]"+"."+jsonConvMappings[rt] if rt in cppClassMappings else rt+"::from_json("+jsn+"[i]"+")"
                f.write(pd+pd+ft+' val;\n')
                f.write(pd+pd+'for(int i = 0; i < '+jsn+'.size(); i++) val.push_back('+cv+');\n')
                f.write(pd+pd+'result->'+n+' = val;\n')
                f.write(pd+'}\n')
            else:
                cv = jsn+"."+jsonConvMappings[rt] if rt in cppClassMappings else rt+"::from_json("+jsn+")"
                f.write(pd+'result->'+n+' = ' + cv + ';\n')

        f.write(pd+'std::shared_ptr<'+obj.name+'> ptr(result);\n')
        f.write(pd+'return ptr;\n')
        f.write('}\n\n')


def write_methods(folder, name, objs, is_struct, is_header):

    f = open(folder + "/" + name + (".hpp" if is_header else ".cpp"), 'w+')

    pd = "  " if is_header else ""

    f.write("#include <memory>\n")
    if is_header:
        f.write('#include <string>\n')
        f.write('#include <vector>\n')
        f.write('\n')
        f.write('#include "model.hpp"\n')
        f.write('\n')
        f.write('#pragma once\n')
        f.write('\n')

        if is_struct:
            f.write("struct "+name+" {\n")
        else:
            f.write("namespace "+name+" {\n")
    else:
        f.write('#include "'+name+'.hpp"\n')
        f.write('\n#include <json_rmi_client.hpp>\n')
        f.write('\nusing namespace '+name+";\n")
        f.write('\nextern JsonRpcClient client;\n')

    for obj in objs:

        if is_header:
            f.write('\n'+pd+'/**\n')
            for dc in obj.descr.split('\n'):
                dc = dc.strip(' ')
                if len(dc) != 0:
                    f.write(pd+" * " + dc + '\n')

            f.write(pd+' * \n')

            for i in range(len(obj.field_names)):
                if obj.field_names[i] == "@return":
                    f.write(pd+' * '+obj.field_names[i] + ' '+obj.field_descs[i] + '\n')
                else:
                    f.write(pd+' * @param ' + obj.field_names[i] + ' '+obj.field_descs[i] + '\n')
            f.write(pd+' */\n')
        else: # is not header
            f.write('\n')

        return_idx = -1
        return_type = "void"
        for i in range(len(obj.field_names)):
            if obj.field_names[i] == "@return":
                return_idx = i
                return_type = get_full_type(obj.field_types[i], obj.field_flags[i])
                break

        prefix = 'virtual ' if is_header and is_struct else ''
        namespace = '' if is_header else name + "::"
        f.write(pd + prefix + return_type + " " + namespace + obj.name + "(")

        for i in range(len(obj.field_names)):
            if obj.field_names[i] != "@return":
                if i > 0:
                    f.write(', ')

                type = get_full_type(obj.field_types[i], obj.field_flags[i])
                f.write(type + ' ' + obj.field_names[i])

        if is_header:
            f.write(');\n')
        else:
            npd = "  "
            f.write(") {\n")
            f.write(npd+'Json::Value args;\n')
            for i in range(len(obj.field_names)):
                if obj.field_names[i] == '@return':
                    continue
                proper_suffix = "" if (obj.field_types[i] in cppClassMappings) else "->to_json()"
                f.write(npd+"args["+str(i)+"] = "+obj.field_names[i]+proper_suffix+";\n")

            if return_idx == -1:
                f.write("\n"+npd+'client.call("'+obj.name+'", args);\n')
            else:
                f.write("\n"+npd+'Json::Value rs = client.call("'+obj.name+'", args);\n')

                f.write("\n"+npd+return_type+' result;\n')
                if conv.FIELD_FLAG_ARRAY in obj.field_flags[return_idx]:
                    rt = obj.field_types[return_idx]
                    cv = "rs[i]."+jsonConvMappings[rt] if rt in cppClassMappings else rt+"::from_json(rs[i])"
                    f.write(npd+'for(int i = 0; i < rs.size(); i++) result.push_back('+cv+');\n')
                else:
                    rt = obj.field_types[return_idx]
                    cv = "rs."+jsonConvMappings[rt] if rt in cppClassMappings else rt+"::from_json(rs)"
                    f.write(npd+"result = "+cv+";\n")

                f.write(npd+'return result;\n')

            f.write("}\n")

    if is_header:
        f.write("}\n")


# ============= #
# Run converter #
# ============= #

# TODO: parse cmdline args

print("Generating output...")
conv.parse_file(src)

print("Writing into folder", dst)
if not os.path.exists(dst): os.makedirs(dst)

write_model_header(dst)
write_model_body(dst)

write_methods(dst, "methods", conv.methods, False, True)
write_methods(dst, "methods", conv.methods, False, False)
write_methods(dst, "events", conv.events, True, True)
