# objects

## Post
Пост в бложике

| Name           | Type | Desc
| ---            | ---  | ---
| blog           | str  | Имя блога?
| editable       | bool | Может ли текущий пользователь изменять пост
| id             | long | Идентификатор поста
| source         | long | Идентификатор источника
| title          | str  | Заголовок поста
| post           | str  | Текст поста, отформатирован в html или md
| time           | long | Время публикации
| author_id      | long | Айдишник автора
| author_pubname | str  | Имя автора, отображаемое на посте (TODO: убрать)
| header_image   | str  | Имя файла изображения заголовка



# methods

## listPosts
Запрашиваем посты в блоге, отсортированные по дате

| Name    | Type   | Desc
| ---     | ---    | ---
| blog    | str    | Имя блога
| count   | long   | Количество постов
| offset  | long   | Смещение от начала
| asc     | bool   | true = по возрастанию
| @return | Post[] | список постов


