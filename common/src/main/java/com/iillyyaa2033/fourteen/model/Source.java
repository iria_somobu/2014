package com.iillyyaa2033.fourteen.model;

public class Source {

    public long id;
    public String name;

    public Source(long id, String name){
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "#"+id +" "+ name;
    }
}
