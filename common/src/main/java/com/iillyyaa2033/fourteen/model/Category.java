package com.iillyyaa2033.fourteen.model;

public class Category {

    public long categoryId;
    public String codename;
    public String categoryName;

    public Category(long id, String name, String codename){
        this.categoryId = id;
        this.categoryName = name;
        this.codename = codename;
    }

    public Category(String codename, String name){
        this.codename = codename;
        this.categoryName = name;
    }

    @Override
    public String toString() {
        return codename+": "+categoryName;
    }
}
