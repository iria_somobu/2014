package com.iillyyaa2033.fourteen.model;

public class Methods {

    public static final String VIEW_BLOG = "posts";

    /**
     * View post by id
     * <br/>
     * <br/>Id must be passed in fragment
     */
    public static final String VIEW_POST = "post";

    /**
     * View all posts in given time range.
     * <br/>
     * <br/>Possible params:
     * <br/>mode=[day, month]
     * <br/>since=[time millis]
     */
    public static final String VIEW_DATE = "by_time";
    public static final String VIEW_USER = "by_user";
    public static final String VIEW_CATEGORY = "by_category";

    public static final String LIST_BLOGS = "list_blogs";

    public static final String ADD_POST = "add_post";

    public static final String EDIT_POST = "edit_post";
    public static final String EDIT_BLOG = "edit_blog";

}
