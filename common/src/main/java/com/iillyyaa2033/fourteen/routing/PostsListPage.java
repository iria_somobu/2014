package com.iillyyaa2033.fourteen.routing;


import com.iillyyaa2033.fourteen.api.Post;

import java.util.List;

public class PostsListPage extends Page {

    public long page;
    public long total;
    public List<Post> posts;

    public Message message = Message.NONE;
    public long datefilter = 0;
    public String catname = null;

    public enum Message {
        NONE,
        FILTER_BY_DAY,
        FILTER_BY_MONTH,
        FILTER_BY_CATEGORY
    }
}
