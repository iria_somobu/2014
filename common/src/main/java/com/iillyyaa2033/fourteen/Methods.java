package com.iillyyaa2033.fourteen;

import com.iillyyaa2033.fourteen.api.IMethods;
import com.iillyyaa2033.fourteen.api.Post;

import java.sql.SQLException;

public class Methods implements IMethods {

    @Override
    public Post[] listPosts(String blog, long count, long offset, boolean asc) {
        Post[] posts;
        try {
            posts = Platform.database(blog).queryPosts(offset).toArray(new Post[0]);
        } catch (SQLException throwables) {
            throw new RuntimeException("Database error");
        }
        return new Post[0];
    }
}
