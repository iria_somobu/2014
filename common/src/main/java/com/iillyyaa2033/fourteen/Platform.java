package com.iillyyaa2033.fourteen;

import java.sql.SQLException;
import java.util.HashMap;

public class Platform {

    private static IPlatform implementation = null;
    private final static HashMap<String, Database> databases = new HashMap<>();

    public static void setImplementation(IPlatform impl) {
        implementation = impl;
    }

    public static IPlatform get() {
        return implementation;
    }

    public static synchronized Database database(String name) throws SQLException {
        if (!databases.containsKey(name)) {
            databases.put(name, Database.open(name));
        }

        return databases.get(name);
    }

    public static void closeDb(String name) throws SQLException {
        database(name).close();
        databases.remove(name);
    }
}
