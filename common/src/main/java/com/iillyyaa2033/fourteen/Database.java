package com.iillyyaa2033.fourteen;

import com.iillyyaa2033.fourteen.api.Post;
import com.iillyyaa2033.fourteen.model.Category;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Database {

    private static final int POSTS_PER_PAGE = 10;

    public boolean editable = true;
    public final String blog;
    private final Connection conn;

    private Database(String blog, Connection conn) {
        this.conn = conn;
        this.blog = blog;
    }

    public static Database open(String blogname) throws SQLException {
        return Updater.getDb(blogname);
    }

    public void close() throws SQLException {
        conn.close();
    }

    private void execSQL(String sql) throws SQLException {
        conn.createStatement().execute(sql);
    }

    public void updateSetting(String key, String value) throws SQLException {
        conn.createStatement().execute("replace into settings(pkey, pval) values ('" + key + "', '" + value + "')");
    }

    public String getSetting(String name) throws SQLException {
        ResultSet rs = conn.createStatement().executeQuery("select pval from settings " +
                "where pkey = '" + name + "';");
        return rs.next() ? rs.getString(1) : null;
    }

    private static final String SELECT_POSTS =
            "select posts._id as post_id, source as post_source, title as post_title, post as post_data, " +
                    "time as post_time, author as post_author, authors.public_name as author_name, " +
                    " header_img as post_header" +
                    " from posts" +
                    " left join authors on posts.author = authors._id ";

    private Post postFromResultSet(ResultSet rs) throws SQLException {
        Post post = new Post();

        post.blog = blog;
        post.editable = editable;
        post.id = rs.getLong(rs.findColumn("post_id"));
        post.source = rs.getLong(rs.findColumn("post_source"));
        post.title = rs.getString(rs.findColumn("post_title"));
        post.post = rs.getString(rs.findColumn("post_data"));
        post.time = rs.getLong(rs.findColumn("post_time"));
        post.author_id = rs.getLong(rs.findColumn("post_author"));
        post.author_pubname = rs.getString(rs.findColumn("author_name"));
        post.header_image = rs.getString(rs.findColumn("post_header"));

//        post.cats = getAssignedCategoriesByPost(post.id);

        return post;
    }

    public List<Post> queryPosts(long page) throws SQLException {
        ArrayList<Post> posts = new ArrayList<>();

        ResultSet rs = conn.createStatement().executeQuery(SELECT_POSTS +
                " order by time DESC limit " + POSTS_PER_PAGE + " offset " + (page - 1) * POSTS_PER_PAGE);

        while (rs.next()) posts.add(postFromResultSet(rs));

        return posts;
    }

    public List<Post> queryPostsByTimeSince(long start, long len, long page) throws SQLException {
        ArrayList<Post> posts = new ArrayList<>();

        ResultSet rs = conn.createStatement().executeQuery(SELECT_POSTS +
                " where time >= " + start + " and time <= " + (start + len) +
                " order by time DESC limit " + POSTS_PER_PAGE + " offset " + (page - 1) * POSTS_PER_PAGE);

        while (rs.next()) posts.add(postFromResultSet(rs));

        return posts;
    }

    public List<Post> queryPostsByCategory(long cat, long page) throws SQLException {
        ArrayList<Post> posts = new ArrayList<>();

        ResultSet rs = conn.createStatement().executeQuery(
                "select posts._id, posts.source, posts.title, posts.post, posts.time, posts.author, authors.public_name, posts.header_img " +
                        " from posts2cats " +
                        " join posts on posts2cats.post = posts._id " +
                        " join authors on posts.author = authors._id " +
                        " where category = " + cat +
                        " order by posts.time DESC limit " + POSTS_PER_PAGE + " offset " + (page - 1) * POSTS_PER_PAGE);

        while (rs.next()) posts.add(postFromResultSet(rs));

        return posts;
    }

    public Post getPostById(long id) throws SQLException {
        ResultSet rs = conn.createStatement().executeQuery(SELECT_POSTS + " where posts._id = " + id + " limit 1");
        return rs.next() ? postFromResultSet(rs) : null;
    }

    public Post getPostBefore(long time) throws SQLException {
        ResultSet rs = conn.createStatement().executeQuery(SELECT_POSTS + " where time < " + time
                + " order by time desc limit 1");
        return rs.next() ? postFromResultSet(rs) : null;
    }

    public Post getPostAfter(long time) throws SQLException {
        ResultSet rs = conn.createStatement().executeQuery(SELECT_POSTS + " where time > " + time
                + " order by time asc limit 1");
        return rs.next() ? postFromResultSet(rs) : null;
    }

    public Category[] getAssignedCategoriesByPost(long postId) throws SQLException {
        ArrayList<Category> result = new ArrayList<>();

        ResultSet rs = conn.createStatement().executeQuery("select categories._id, categories.codename, categories.name" +
                " from posts2cats" +
                " inner join categories on posts2cats.category = categories._id" +
                " where post = " + postId);

        while (rs.next()) {
            result.add(new Category(rs.getLong(1), rs.getString(2), rs.getString(3)));
        }

        return result.toArray(new Category[0]);
    }

    public String getCatName(long cat_id) throws SQLException {
        ResultSet rs = conn.createStatement().executeQuery("select categories.name from categories" +
                " where categories._id = '" + cat_id + "'");
        return rs.next() ? rs.getString(1) : null;
    }

    static class Updater {

        private static final String CREATE_TABLE_POSTS = "create table if not exists posts(" +
                "_id integer primary key autoincrement, " +
                "source integer not null default 0, " +
                "title text not null, " +
                "post text not null, " +
                "time integer not null, " +
                "header_img text," +
                "author integer not null default 1);";

        private static final String CREATE_TABLE_AUTHORS = "create table if not exists authors(" +
                "_id integer primary key autoincrement, " +
                "internal_name text not null, " +
                "public_name text not null);";


        private static final String CREATE_TABLE_TAGS = "create table if not exists tags(" +
                "_id integer primary key autoincrement," +
                "name text not null, " +
                "codename text);";

        private static final String CREATE_TABLE_POSTS2TAGS = "create table if not exists posts2tags(" +
                "post integer not null," +
                "tag integer not null);";


        private static final String CREATE_TABLE_CATS = "create table if not exists categories(" +
                "_id integer primary key autoincrement, " +
                "codename text, " +
                "name text not null);";

        private static final String CREATE_TABLE_POSTS2CATS = "create table if not exists posts2cats(" +
                "post integer not null," +
                "category integer not null);";


        private static final String CREATE_TABLE_SETTINGS = "create table if not exists settings(" +
                "pkey text not null primary key," +
                "pval text not null);";

        private static final String CREATE_TABLE_POSTCOUNTER = "create table if not exists postcounter(" +
                "year integer not null, " +
                "month integer not null," +
                "day integer not null," +
                "cnt integer not null" +
                ");";

        private static final String CREATE_TABLE_SOURCES = "create table if not exists sources(" +
                "_id integer primary key autoincrement, " +
                "name text not null" +
                ");";

        private static final String VERSION_KEY = "db_ver";
        private static final int CURRENT_DATABASE_VERSION = 7;

        static Database getDb(String blogname) throws SQLException {
            Connection conn = DriverManager.getConnection(Platform.get().getJdbcPath(blogname));
            Database db = new Database(blogname, conn);

            checkCreateSettings(db);

            String verStr = db.getSetting(VERSION_KEY);
            if (verStr == null) {
                onCreate(db);
                db.updateSetting(VERSION_KEY, "" + CURRENT_DATABASE_VERSION);
            } else {
                int ver = Integer.parseInt(verStr); // Can throw exception but I don't care
                if (ver < CURRENT_DATABASE_VERSION) {
                    onUpdate(db, ver);
                    db.updateSetting(VERSION_KEY, "" + CURRENT_DATABASE_VERSION);
                }
            }

            return db;
        }

        private static void checkCreateSettings(Database db) throws SQLException {
            db.execSQL(CREATE_TABLE_SETTINGS);
        }

        private static void onCreate(Database db) throws SQLException {
            db.execSQL(CREATE_TABLE_POSTS);
            db.execSQL(CREATE_TABLE_AUTHORS);
            db.execSQL(CREATE_TABLE_TAGS);
            db.execSQL(CREATE_TABLE_POSTS2TAGS);
            db.execSQL(CREATE_TABLE_CATS);
            db.execSQL(CREATE_TABLE_POSTS2CATS);
            db.execSQL(CREATE_TABLE_POSTCOUNTER);
            db.execSQL(CREATE_TABLE_SOURCES);
        }

        static void onUpdate(Database db, int from) {
            switch (from) {
                case 7: // Updating from v7
                    break;
            }
        }

    }
}
