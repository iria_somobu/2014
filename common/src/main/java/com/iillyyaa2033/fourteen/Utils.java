package com.iillyyaa2033.fourteen;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.Map;

public class Utils {

    public static final String URL = "<a href=\"%s\">%s</a>";

    public static boolean isDbEditable(String name){
        if(name.equals(Const.LOCAL_BLOG)) return true;

        File file = new File(Const.EXT_ROOT, name + "/.editable");

        return file.exists();
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static URI getUri(String auth, String path, String query, String fragment){
        String full = "fourteen://";
        full += auth;
        if(path != null) full += "/" + path;
        if(query != null) full += "?" + query;
        if(fragment != null) full += "#" + fragment;
        return URI.create(full);
    }

    public static URI getUri(String auth, String path, long fragment){
        String full = "fourteen://";
        full += auth;
        if(path != null) full += "/" + path;
        full += "#" + fragment;
        return URI.create(full);
    }

    public static long parseLong(String sq, int fallback){
        try{
            return Long.parseLong(sq);
        } catch(Exception e){
            return fallback;
        }
    }

    public static Map<String, String> splitQuery(URI url) {
        Map<String, String> query_pairs = new LinkedHashMap<>();

        String query = url.getQuery();
        String[] pairs = query.split("&");

        for (String pair : pairs) {
            int idx = pair.indexOf("=");

            try {
                query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        return query_pairs;
    }

}
