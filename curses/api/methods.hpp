#include <memory>
#include <string>
#include <vector>

#include "model.hpp"

#pragma once

namespace methods {

  /**
   * Запрашиваем посты в блоге, отсортированные по дате
   * 
   * @param blog Имя блога
   * @param count Количество постов
   * @param offset Смещение от начала
   * @param asc true = по возрастанию
   * @return список постов
   */
  std::vector<std::shared_ptr<Post>> listPosts(std::string blog, long count, long offset, bool asc);
}
