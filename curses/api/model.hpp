#include <string>
#include <vector>
#include <json.h>

#pragma once

struct Post;

/**
 */
struct Post {

  /**
   * Имя блога?
   */
  std::string blog;

  /**
   * Может ли текущий пользователь изменять пост
   */
  bool editable;

  /**
   * Идентификатор поста
   */
  long id;

  /**
   * Идентификатор источника
   */
  long source;

  /**
   * Заголовок поста
   */
  std::string title;

  /**
   * Текст поста, отформатирован в html или md
   */
  std::string post;

  /**
   * Время публикации
   */
  long time;

  /**
   * Айдишник автора
   */
  long author_id;

  /**
   * Имя автора, отображаемое на посте (TODO: убрать)
   */
  std::string author_pubname;

  /**
   * Имя файла изображения заголовка
   */
  std::string header_image;

  Json::Value to_json();

  static std::shared_ptr<Post> from_json(Json::Value& json);

};


