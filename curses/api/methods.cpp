#include <memory>
#include "methods.hpp"

#include <json_rmi_client.hpp>

using namespace methods;

extern JsonRpcClient client;

std::vector<std::shared_ptr<Post>> methods::listPosts(std::string blog, long count, long offset, bool asc) {
  Json::Value args;
  args[0] = blog;
  args[1] = count;
  args[2] = offset;
  args[3] = asc;

  Json::Value rs = client.call("listPosts", args);

  std::vector<std::shared_ptr<Post>> result;
  for(int i = 0; i < rs.size(); i++) result.push_back(Post::from_json(rs[i]));
  return result;
}
