#include <cstdio>
#include <ncurses.h>
#include <string>
#include <vector>
#include <api/model.hpp>
#include <json_rmi_client.hpp>

#define COLOR_PAIR_SCREEN 1
#define COLOR_PAIR_WINDOW 2

std::vector<std::shared_ptr<Post>> page;

JsonRpcClient client("/tmp/fourteen-core.sock");

void entry_list();

int main() {
    for (int i = 0; i < 50; i++) {
        Post* post = new Post();
        post->title = "Journal entry #" + std::to_string(i);
        post->time = 0;
        page.push_back(std::shared_ptr<Post>(post));
    }

    initscr();
    clear();
    noecho();
    cbreak();

    start_color();
    init_pair(COLOR_PAIR_SCREEN, COLOR_BLUE, COLOR_RED);
    init_pair(COLOR_PAIR_WINDOW, COLOR_BLACK, COLOR_WHITE);

    entry_list();

    clrtoeol();
    refresh();
    endwin();

    return 0;
}

void entry_list() {
    int row, col;
    getmaxyx(stdscr, row, col);

    const int paddingx = 4;
    const int paddingy = 1;

    int startx = paddingx;
    int starty = paddingy;

    int max_entries = row - 2*paddingy - 2;

    WINDOW *menu_win = newwin(row - 2 * paddingy, col - 2 * paddingx, starty, startx);
    keypad(menu_win, TRUE);
    wbkgd(menu_win, COLOR_PAIR(COLOR_PAIR_WINDOW));

    int offset = 0;
    int highlight = 1;
    int choice = 0;
    int c;

    do {
        werase(menu_win);

        int x = 2;
        int y = 1;

        if(highlight < offset + 1) offset = highlight - 1;
        if(highlight > offset + max_entries) offset = highlight - max_entries;

        for (unsigned long i = offset; i < page.size() || i < offset + max_entries; ++i) {
            std::string text = "page[i]->time | " + page[i]->title;

            if (highlight == i + 1) {
                wattron(menu_win, A_REVERSE);
                mvwprintw(menu_win, y, x, "%s", text.c_str());
                wattroff(menu_win, A_REVERSE);
            } else {
                mvwprintw(menu_win, y, x, "%s", text.c_str());
            }
            ++y;
        }
        box(menu_win, 0, 0);
        wrefresh(menu_win);


        c = wgetch(menu_win);

        switch (c) {
            case KEY_UP:
                if (highlight == 1) highlight = page.size();
                else --highlight;
                break;
            case KEY_DOWN:
                if (highlight == page.size()) highlight = 1;
                else ++highlight;
                break;
            case 10:
                choice = highlight;
                break;
            default:
                break;
        }

        if (choice != 0) break;
    } while (true);
}


